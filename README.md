# Microkinetic modelling of oscillations in the NO + CO reaction on Pt(100)

*M.M. van der Horst; L.H.E. Kempen; D. van Krimpen; M.G.A. Nooijens; N.A.H.
Theunissen*

Supplementary materials for the microkinetics assignment of the course 6EMAC2
(Modern concepts in catalysis) at Eindhoven University of Technology, 2020.

This repository contains all input files and scripts used to produce the
results shown in the associated report.

[Click here to download this entire repository as a zip file.](https://gitlab.com/lkkmpn/ertl/-/archive/master/ertl-master.zip)

## Repository layout

The repository contains two directories: `mkmcxx` and `vasp`. Both directories
contain the input files for the respective software packages, as well as some
utility scripts for running the software packages and processing their output.
Details on how to reproduce the results are given in this file, `README.md`.

All Python scripts require Python > 3.7. The Python scripts for the MKMCXX
simulations also require `matplotlib`, `numpy`, and `scipy`.

## MKMCXX

In this work, MKMCXX was used to perform the microkinetic simulations. The
`mkmcxx` directory contains a set of Python scripts, which can be used to make
running the simulations easier. The `run_mkmcxx.py` script runs all simulations
that were performed for this work. The other scripts contain functions used by
the `run_mkmcxx.py` script. The `input` directory contains the `.mkm` input
files, encoding the actual simulations. Note that some input files are template
files which cannot be run directly by MKMCXX, but need preprocessing. This is
automatically done by the Python scripts.

To recreate the calculations performed in this work, place the MKMCXX binary in
the `bin` directory and run the `run_mkmcxx.py` script. This script will
automatically perform all MKMCXX calculations and create the plots used in the
work. However, more detailed instructions are included below. These
instructions will only provide you with information to run the MKMCXX
simulations. Plotting can then be done using the `coverage.dat` files produced
by MKMCXX.

* Run 1: exploring the effect of temperature on oscillations.
  1. This is one of the simplest runs to execute manually. Execute the
  following command: `bin/mkmcxx -i input/input-exploreT.mkm`.
* Run 2: perform a sequence of various temperatures.
  1. This run is also simple: `bin/mkmcxx -i input/input-sequence.mkm`.
  However, creating the plot requires manual concatenation of the four
  `coverage.dat` files.
* Run 3: compare large vs small time steps.
  1. Perform the simulation using the default time step behaviour:
  `bin/mkmcxx -i input/input-412K-single.mkm`.
  2. Open `input/input-412K-repeat.mkm`, and replace all variables between
  angle brackets (`<species>`) on lines 7–11 with their initial coverages
  (`0.0` for every species except `*`, which is `1.0`).
  3. Run: `bin/mkmcxx -i input/input-412K-repeat.mkm`.
  4. Extract `run/412K/coverage.dat`, and copy the coverages from the last line
  of this file to `input/input-412K-repeat.mkm`, to use as new initial
  coverages.
  5. Go to step 3, repeat 200 times.
* Run 4: explore the effect of pressure.
  1. Open `input/input-template-P.mkm`, and replace `<P>` on line 52 by a
  pressure between `3e-10` and `30e-10`.
  2. Run: `bin/mkmcxx -i input/input-template-P.mkm`.
  3. Extract `run/412K/coverage.dat` and `run/422K/coverage.dat`.
  4. Go to step 1, repeat until all pressures have been enumerated.
* Run 5: explore the effect of NO : CO gas phase ratio.
  1. Open `input/input-template-ratio.mkm`, and replace `<NO>` on line 5 by a
  concentration ratio between `0.333` and `4`.
  2. Run: `bin/mkmcxx -i input/input-template-ratio.mkm`.
  3. Extract `run/412K/coverage.dat`.
  4. Go to step 1, repeat until all ratios have been enumerated.
* Run 6: explore the effect of theta_UB for surface reactions.
  1. Open `input/input-template-UB.mkm`, and replace `<UB>` on lines 44–45 by a
  value between `0.35` and `1.0`.
  2. Run: `bin/mkmcxx -i input/input-template-UB.mkm`.
  3. Extract `run/412K/coverage.dat`.
  4. Go to step 1, repeat until all values have been enumerated.

The simulations in this work have been performed using MKMCXX version 2.15.3.
Note that this is a relatively new version of MKMCXX, which is required for
some functionality to work. Make sure you have downloaded the newest version of
MKMCXX from [the MKMCXX website](https://www.mkmcxx.nl).

## VASP

In this work, VASP was used to calculate the vibrational frequencies of adsorbed
CO and NO. The `vasp` directory contains four sets of files, which were used for
these calculations. The `CO-Pt100` and `NO-Pt100` directories contain input
files for the relaxation simulation (respectively for CO on Pt(100) and NO on
Pt(100)), and the `CO-Pt100-vib` and `NO-Pt100-vib` directories contain input
files for the vibrational frequency calculations.

The `vasp` folder also contains two Python scripts:

* `cleanup_vasp.py`: Cleanup the VASP folders by removing all output files.
* `run_vasp.py`: Run the VASP simulations in the correct order, automatically
transferring files as needed (see below).

To recreate the calculations performed in this work, perform the following
steps:

1. Place a VASP binary in the `vasp/bin` folder. Check whether the name of the
binary corresponds to the value of the `vasp` variable in the `vasp/run_vasp.py`
script (if you want to use this script for an automated run).
2. Run the Python script `vasp/run_vasp.py`.
3. The `OUTCAR` files in the `CO-Pt100-vib` and `NO-Pt100-vib` directories now
contain the vibrational frequencies (eigenvalues of the Hessian matrix), which
can be used to calculate the vibrational partition functions.

If you want to perform the steps done by the Python script in step 2 manually,
perform the following steps:
1. Execute the `CO-Pt100` simulation by running the VASP binary from that
directory.
2. As the output of the `CO-Pt100` simulation forms the input of the
`CO-Pt100-vib` simulation, copy `CO-Pt100/CONTCAR` to `CO-Pt100-vib/POSCAR`.
3. Freeze the Pt atoms by changing `T` to `F` on lines 13–15 in
`CO-Pt100-vib/POSCAR`.
4. Execute the `CO-Pt100-vib` simulation by running the VASP binary from that
directory.
5. Repeat the previous 4 steps for the NO simulations.

The simulations in this work have been performed using VASP version 5.4.1.
