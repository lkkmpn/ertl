import logging
import numbers
import os


logger = logging.getLogger('mkmcxx')


def create_input_file(template_file, out_file, replace_values, enforce_positive=False):
    """Create an MKMCXX input file from a template file. A template file can contain variables, enclosed in angle
    brackets (< and >). These variables are defined in the replace_values dict (without brackets).

    Arguments:
        template_file {str} -- Location of the template mkm file.
        out_file {str} -- Location of the output file.
        replace_values {dict} -- Dictionary with variables in the template file and values which they should have.

    Keyword Arguments:
        enforce_positive {bool} -- Enforce that the input variables should be positive. (default: {False})
    """

    if not os.path.isfile(template_file):
        logger.exception(f'mkm_template: {input_file} is not a file.')

    with open(template_file, 'r') as f:
        template = f.read()

    for key, val in replace_values.items():
        if enforce_positive and isinstance(val, numbers.Real) and val < 0:
            val = 0
        template = template.replace(f'<{key}>', str(val))

    with open(out_file, 'w') as f:
        f.write(template)

    logger.debug(f'mkm_template: input file {out_file} created.')


if __name__ == '__main__':
    sys.exit('This Python module cannot be executed from the console.')
