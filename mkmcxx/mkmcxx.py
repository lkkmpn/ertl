import itertools
import logging
import os
import shutil
import subprocess
import sys
import numpy as np

import mkm_template
import parse_output


os.chdir(os.path.dirname(os.path.abspath(__file__)))
mkmcxx = 'bin/mkmcxx'

logger = logging.getLogger('mkmcxx')


def run(input_file, output_dir=None):
    """Run an MKMCXX input file, ensuring a clean environment, and move the output to a different directory if desired.
    Note that this function only works if USETIMESTAMP = 0 in the MKMCXX input file, so that the output is written to
    the 'run' directory.

    Arguments:
        input_file {str} -- Location of the input mkm file.

    Keyword Arguments:
        output_dir {str} -- Location of the desired output directory. If None, the directory is not moved.
        (default: {None})
    """

    if not os.path.isfile(mkmcxx):
        logger.exception(f'mkmcxx.run: MKMCXX path {mkmcxx} not valid.')

    if not os.path.isfile(input_file):
        logger.exception(f'mkmcxx.run: {input_file} is not a file.')

    if os.path.isdir('run'):
        shutil.rmtree('run', ignore_errors=True)
        logger.debug('mkmcxx.run: removed old run directory.')

    if output_dir is not None and os.path.isdir(output_dir):
        shutil.rmtree(output_dir, ignore_errors=True)
        logger.debug(f'mkmcxx.run: removed old {output_dir} directory.')

    proc = subprocess.run([mkmcxx, '-i', os.path.abspath(input_file)],
                          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    if proc.returncode != 0:
        logger.exception('mkmcxx.run: MKMCXX returned an error during this run. Output:\n' +
                         proc.stdout.decode('utf-8'))

    if output_dir is not None:
        shutil.move('run', output_dir)

    logger.debug(f'mkmcxx.run: executed {input_file}.')


def run_repeat(input_file, output_dir, num_iterations, initial_coverages):
    """Run an MKMCXX input file repeatedly, using the coverages at the end of the previous run as initial coverages for
    the next run. This function requires a template file which can be parsed by mkm_template.create_input_file. The
    function writes a concatenated coverage_seq.dat file.

    Arguments:
        input_file {str} -- Location of the template mkm file.
        output_dir {str} -- Location of the desired output directory.
        num_iterations {int} -- Number of iterations to perform.
        initial_coverages {dict} -- Dictionary of initial coverages to start with.
    """

    if not os.path.isfile(input_file):
        logger.exception(f'mkmcxx.run_repeat: {input_file} is not a file.')

    if os.path.isdir(output_dir):
        shutil.rmtree(output_dir, ignore_errors=True)

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    coverages = initial_coverages
    cumulative_time = 0
    coverage = None
    header = None

    for i in range(num_iterations):
        mkm_file = os.path.join(output_dir, 'input-temp.mkm')
        mkm_template.create_input_file(input_file, mkm_file, coverages, enforce_positive=True)

        run(mkm_file)

        temperatures, times = parse_output.parse_run_mkm('run')

        coverage_file = os.path.join('run', f'{temperatures[0]:.6g}K', 'coverage.dat')
        shutil.copyfile(coverage_file, os.path.join(output_dir, f'coverage_{i}.dat'))
        header, this_coverage, ads_idx = parse_output.parse_coverage_file(coverage_file)

        coverages = {header[j]: this_coverage[-1, j] for j in ads_idx}

        if i == 0:
            # initialise coverage array
            coverage = this_coverage.copy()
        else:
            # add to coverage array
            this_coverage[:, 0] += cumulative_time
            coverage = np.concatenate((coverage, this_coverage))

        cumulative_time += times[0]

        logger.debug(f'mkmcxx.run_repeat: performed run {i + 1}.')

    np.savetxt(os.path.join(output_dir, 'coverage_seq.dat'), coverage,
               delimiter='\t', header='\t'.join(header), comments='')

    logger.debug(f'mkmcxx.run_repeat: written sequence coverage file.')


def run_batch(input_file, output_dir, variables_values):
    """Run a batch of MKMCXX input files, using a template input file. This function requires a template file which can
    be parsed by mkm_template.create_input_file.

    Arguments:
        input_file {str} -- Location of the template mkm file.
        output_dir {str} -- Location of the desired output directory.
        variables_values {dict} -- Dictionary with variables in the template file and values which they should have.
    """

    if not os.path.isfile(input_file):
        logger.exception(f'mkmcxx.run_batch: {input_file} is not a file.')

    if os.path.isdir(output_dir):
        shutil.rmtree(output_dir, ignore_errors=True)

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    variables = variables_values.keys()
    values = variables_values.values()

    combinations = list(itertools.product(*values))

    for i, combination in enumerate(combinations):
        logger.info(f'Running combination {i + 1} of {len(combinations)}...')

        replace_values = dict(zip(variables, combination))
        mkm_file = os.path.join(output_dir, 'input-temp.mkm')
        mkm_template.create_input_file(input_file, mkm_file, replace_values)

        run(mkm_file)

        # move run data to desired output directory
        for this_dirname in os.listdir('run'):
            if not this_dirname.endswith('K'):
                continue

            src = os.path.join('run', this_dirname)
            dst = os.path.join(output_dir, '_'.join(combination) + '_' + this_dirname)
            shutil.move(src, dst)


if __name__ == '__main__':
    sys.exit('This Python module cannot be executed from the console.')
