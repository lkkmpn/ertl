import sys
import matplotlib.pyplot as plt
import matplotlib


def apply(plot_type='png'):
    """Apply desired matplotlib settings.

    Keyword Arguments:
        plot_type {str} -- Either 'png' or 'pgf'. 'pgf' sets up LaTeX for the text parsing, and other custom
        configuration settings. 'png' is faster.
        (default: {'png'})
    """

    if plot_type not in ['png', 'pgf']:
        plot_type = 'png'

    plt.style.use('seaborn-muted')

    if plot_type == 'png':
        rc_params = {
            'savefig.dpi': 225
        }
        matplotlib.rcParams.update(rc_params)

    if plot_type == 'pgf':
        matplotlib.use('pgf')
        rc_params = {
            'font.family': 'XCharter',
            'font.size': 12,
            'legend.columnspacing': 1,
            'legend.edgecolor': '0',
            'legend.fancybox': False,
            'legend.handlelength': 1.5,
            'legend.handletextpad': .6,
            'text.usetex': True,
            'patch.linewidth': .5,
            'pgf.rcfonts': False,
            'pgf.texsystem': 'pdflatex',
            'pgf.preamble': [
                r'\usepackage{siunitx}',
                r'\usepackage[version=4]{mhchem}',
                r'\usepackage[bitstream-charter]{mathdesign}',
                r'\usepackage[T1]{fontenc}'
            ],
            'savefig.dpi': 225
        }
        matplotlib.rcParams.update(rc_params)


if __name__ == '__main__':
    sys.exit('This Python module cannot be executed from the console.')
