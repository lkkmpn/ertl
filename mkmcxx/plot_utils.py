import logging
import multiprocessing
import os
import sys
import matplotlib.pyplot as plt
import matplotlib
import numpy as np

import parse_output


logger = logging.getLogger('mkmcxx')


def plot_coverage(run_dir):
    """Plot the coverage as function of time for an entire MKMCXX run.

    Arguments:
        run_dir {str} -- The directory in which the MKMCXX output files are contained.
    """

    if not os.path.isdir(run_dir):
        logger.exception(f'parse_run_mkm: {run_dir} is not a directory.')

    if os.path.isfile(os.path.join(run_dir, 'input.mkm')):
        temperatures, _ = parse_output.parse_run_mkm(run_dir)
        datasets = [f'{temperature:.6g}K' for temperature in temperatures]
    else:
        subdirs = os.listdir(run_dir)
        datasets = []
        for subdir in subdirs:
            if subdir.endswith('K'):
                datasets.append(subdir)

    out_dir = os.path.join(run_dir, 'linear-plots')
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    args = []
    for dataset in datasets:
        coverage_file = os.path.join(run_dir, dataset, 'coverage.dat')
        out_file = os.path.join(out_dir, f'{dataset}.png')
        site_symbol = '*'

        args.append([coverage_file, out_file, site_symbol, dataset])

    pool = multiprocessing.Pool()
    logger.debug(f'plot_coverage: plotting using {pool._processes} processes.')
    pool.starmap(plot_coverage_single, args)


def plot_coverage_single(coverage_file, out_file, site_symbol, title):
    """Plot the coverage as function of time for a single coverage.dat file.

    Arguments:
        coverage_file {str} -- Location of coverage.dat file.
        out_file {str} -- Target location of output plot file.
        site_symbol {str} -- Symbol used for adsorbed sites.
        title {str} -- Title for the plot.
    """

    if not os.path.isfile(coverage_file):
        logger.exception(f'plot_coverage_single: {coverage_file} is not a file.')

    header, coverage, ads_idx = parse_output.parse_coverage_file(coverage_file, site_symbol)

    fig, ax = plt.subplots(figsize=(7, 1.4))

    plot_to_ax(ax, header, coverage, ads_idx)

    fig.savefig(out_file, bbox_inches='tight')
    plt.close()

    logger.debug(f'plot_coverage_single: Written plot to {out_file}.')


def plot_coverage_sequence(run_dir, out_file='sequence'):
    """Plot the coverage as function of time for an MKMCXX sequence (created using SEQAL = 1).

    Arguments:
        run_dir {str} -- The directory in which the MKMCXX output files are located.

    Keyword Arguments:
        out_file {str} -- The name of the output plot file, without extension. (default: {'sequence'})
    """

    if not os.path.isdir(run_dir):
        logger.exception(f'parse_run_mkm: {run_dir} is not a directory.')

    temperatures, times = parse_output.parse_run_mkm(run_dir)

    cum_times = np.cumsum(times)

    coverage = None
    ads_idx = None

    for i, temperature in enumerate(temperatures):
        coverage_file = os.path.join(run_dir, f'{temperature:.6g}K', 'coverage.dat')

        header, this_coverage, ads_idx = parse_output.parse_coverage_file(coverage_file)

        if i == 0:
            # initialise coverage array
            coverage = this_coverage.copy()
        else:
            # add to coverage array
            this_coverage[:, 0] += cum_times[i - 1]
            coverage = np.concatenate((coverage, this_coverage))

        logger.debug(f'plot_coverage_sequence: Concatenated run with T = {temperature}')

    fig, ax = plt.subplots(figsize=(7, 2))

    for time in cum_times[:-1]:
        ax.axvline(time, c='grey', ls=':', lw=.5, label='_nolegend_')

    plot_to_ax(ax, header, coverage, ads_idx,
               legend_kwargs={'loc': 'upper right', 'bbox_to_anchor': (.995, .87)},
               xlim=[0, cum_times[-1]])

    for i in range(len(temperatures)):
        time = cum_times[i - 1] if i > 0 else 0
        temperature = temperatures[i]

        x_ax = ax.transAxes.inverted().transform(ax.transData.transform((time, 0)))[0]

        ax.text(x_ax + .005, .975, r'\SI{' + f'{temperature:.6g}' + r'}{\kelvin}', transform=ax.transAxes, va='top')

    fig.savefig(f'{out_file}.png', bbox_inches='tight')
    fig.savefig(f'{out_file}.pgf', bbox_inches='tight')

    logger.debug(f'plot_coverage_sequence: Written plot to {out_file}.')


def plot_to_ax(ax, header, coverage, ads_idx, legend_kwargs={}, xlim=None, xlabel=True, ylabel=True, legend=True):
    """Plot the coverage as function of time to an ax object.

    Arguments:
        ax {matplotlib.axes._subplots.AxesSubplot} -- Matplotlib ax object to draw the coverages to.
        header {np.ndarray} -- numpy array of column titles, returned from parse_output.parse_coverage_file.
        coverage {np.ndarray} -- numpy array of coverage data, returned from parse_output.parse_coverage_file.
        ads_idx {list} -- list containing indices of adsorbed species, returned from parse_output.parse_coverage_file.

    Keyword Arguments:
        legend_kwargs {dict} -- keyword arguments to pass to the legend call. (default: {{}})
        xlim {list} -- x axis limits, or None for the matplotlib defaults. (default: {None})
        xlabel {bool} -- whether to set an x label. (default: {True})
        ylabel {bool} -- whether to set an y label. (default: {True})
        legend {bool} -- whether to add a legend. (default: {True})
    """

    ax.plot(coverage[:, 0], coverage[:, ads_idx])

    if xlabel:
        ax.set_xlabel('Time (s)')
    else:
        ax.tick_params('x', labelbottom=False)

    if ylabel:
        ax.set_ylabel('Coverage')
    else:
        ax.tick_params('y', labelbottom=False)

    if xlim is None:
        ax.set_xlim(0, max(coverage[:, 0]))
    else:
        ax.set_xlim(xlim)

    ax.set_ylim(-.05, 1.05)

    if legend:
        legend_entries = [r'\ce{' + species.replace('*', r'{$\ast$}') + r'}' for species in header[ads_idx]]
        ax.legend(legend_entries, ncol=len(legend_entries), **legend_kwargs)


if __name__ == '__main__':
    sys.exit('This Python module cannot be executed from the console.')
