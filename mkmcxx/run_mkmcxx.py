import logging
import os
import shutil
import subprocess
import sys
import numpy as np
import matplotlib.gridspec
import matplotlib.pyplot as plt
import scipy.signal

import mkmcxx
import mpl_settings
import parse_output
import plot_utils


logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')
logger = logging.getLogger('mkmcxx')
logger.setLevel(logging.INFO)

mpl_settings.apply('pgf')


def run_exploreT():
    """Explore the effect of temperature on oscillations.
    """

    logger.info('Running exploreT...')
    mkmcxx.run('input/input-exploreT.mkm', 'run-exploreT')

    logger.info('Plotting exploreT...')
    plot_utils.plot_coverage('run-exploreT')

    logger.info('Done with exploreT, written figures to run-exploreT/linear-plots/.')


def run_sequence():
    """Perform a sequence of various temperatures.
    """

    logger.info('Running sequence...')
    mkmcxx.run('input/input-sequence.mkm', 'run-sequence')

    if not os.path.isdir('figures'):
        os.mkdir('figures')

    logger.info('Plotting sequence...')
    plot_utils.plot_coverage_sequence('run-sequence', out_file='figures/sequence')

    logger.info('Done with sequence, written figures to figures/.')


def run_repeat():
    """Compare large vs small time steps.
    """

    logger.info('Running repeat comparison...')
    logger.info('(1/2) Single run...')

    mkmcxx.run('input/input-412K-single.mkm', 'run-412K-single')

    logger.info('(2/2) Repeat run...')
    initial_coverages = {
        'CO*': 0.0,
        'NO*': 0.0,
        'N*': 0.0,
        'O*': 0.0,
        '*': 1.0
    }
    mkmcxx.run_repeat('input/input-412K-repeat.mkm', 'run-412K-repeat', 200, initial_coverages)

    logger.info('Plotting repeat run comparison...')

    fig, ax = plt.subplots(2, 1, figsize=(7, 3), sharex=True, gridspec_kw={'hspace': .15})

    header_s, coverage_s, ads_idx_s = parse_output.parse_coverage_file('run-412K-single/412K/coverage.dat')
    plot_utils.plot_to_ax(ax[0], header_s, coverage_s, ads_idx_s, xlabel=False, legend_kwargs={'loc': 'upper right'})

    header_r, coverage_r, ads_idx_r = parse_output.parse_coverage_file('run-412K-repeat/coverage_seq.dat')
    plot_utils.plot_to_ax(ax[1], header_r, coverage_r, ads_idx_r, legend=False)

    # indicate location of O* peaks
    Oads_idx = np.where(header_r == 'O*')[0][0]
    peaks_idx, _ = scipy.signal.find_peaks(coverage_r[:, Oads_idx])
    t_peaks = coverage_r[peaks_idx, 0]

    for t in t_peaks:
        [ax[i].axvline(t, c='grey', ls=':', lw=.5, label='_nolegend_') for i in range(2)]

    for i in range(2):
        ax[i].text(.01, .95, chr(97+i), weight='bold', ha='left', va='top', transform=ax[i].transAxes)

    if not os.path.isdir('figures'):
        os.mkdir('figures')

    fig.savefig('figures/repeat.png', bbox_inches='tight')
    fig.savefig('figures/repeat.pgf', bbox_inches='tight')

    logger.info('Done with repeat, written figures to figures/.')


def run_pressure():
    """Explore the effect of pressure.
    """

    logger.info('Running pressure...')

    P = np.arange(3, 31)

    batch_vars = {
        'P': ['%de-10' % i for i in P]
    }

    mkmcxx.run_batch('input/input-template-P.mkm', 'run-pressure', batch_vars)

    logger.info('Plotting pressure...')
    plot_utils.plot_coverage('run-pressure')

    # find period of O* peaks
    T = [412, 422]
    periods = {t: [] for t in T}

    for p in P:
        for t in T:
            header, coverage, ads_idx = parse_output.parse_coverage_file(f'run-pressure/{p}e-10_{t}K/coverage.dat')

            Oads_idx = np.where(header == 'O*')[0][0]
            peaks_idx, _ = scipy.signal.find_peaks(coverage[:, Oads_idx])
            t_peaks = coverage[peaks_idx, 0]
            period = np.mean(np.diff(t_peaks))
            periods[t].append(period)

    fig = plt.figure(figsize=(7, 2))
    gs = matplotlib.gridspec.GridSpec(2, 2, figure=fig, hspace=.15, wspace=.225, width_ratios=[.8, 1])
    subplots = [gs[:, 0], gs[0, 1], gs[1, 1]]
    ax = [fig.add_subplot(sp) for sp in subplots]

    for t in T:
        ax[0].plot(P / 1e10, scipy.signal.savgol_filter(periods[t], 5, 1), '--', lw=1, c='grey', label='_nolegend_')
        ax[0].plot(P / 1e10, periods[t], 'x', markersize=4)
    ax[0].set_xlabel('Pressure (bar)')
    ax[0].set_ylabel('Oscillation period (s)')
    ax[0].set_xlim(0)

    header_t, coverage_t, ads_idx_t = parse_output.parse_coverage_file(f'run-pressure/7e-10_412K/coverage.dat')
    plot_utils.plot_to_ax(ax[1], header_t, coverage_t, ads_idx_t, xlabel=False, legend_kwargs={'fontsize': 8})

    header_b, coverage_b, ads_idx_b = parse_output.parse_coverage_file(f'run-pressure/25e-10_422K/coverage.dat')
    plot_utils.plot_to_ax(ax[2], header_b, coverage_b, ads_idx_b, legend=False)

    ax[0].annotate('b', (7e-10, periods[412][4]), xytext=(15, 10),
                   textcoords='offset points', arrowprops={'arrowstyle': '->', 'shrinkA': 0, 'shrinkB': 3})
    ax[0].annotate('c', (25e-10, periods[422][21]), xytext=(0, 20),
                   textcoords='offset points', arrowprops={'arrowstyle': '->', 'shrinkA': 0, 'shrinkB': 3})

    ax[0].text(.02, .97, 'a', weight='bold', ha='left', va='top', transform=ax[0].transAxes)
    for i in [1, 2]:
        ax[i].text(.015, .933, chr(97+i), weight='bold', ha='left', va='top', transform=ax[i].transAxes)

    ax[0].legend([r'$T=\SI{412}{\kelvin}$', r'$T=\SI{422}{\kelvin}$'], handlelength=.5)

    if not os.path.isdir('figures'):
        os.mkdir('figures')

    fig.savefig('figures/pressure.png', bbox_inches='tight')
    fig.savefig('figures/pressure.pgf', bbox_inches='tight')

    logger.info('Done with pressure, written figures to figures/.')


def run_ratio():
    """Explore the effect of NO : CO gas phase ratio.
    """

    logger.info('Running ratio...')

    ratios = np.linspace(1/3, 4, 12)

    batch_vars = {
        'NO': ['%.3f' % i for i in ratios]
    }

    mkmcxx.run_batch('input/input-template-ratio.mkm', 'run-ratio', batch_vars)

    logger.info('Plotting ratio...')
    plot_utils.plot_coverage('run-ratio')

    fig, ax = plt.subplots(2, 2, figsize=(7, 2.5), sharex=True, sharey=True, gridspec_kw={'hspace': .15, 'wspace': .1})
    ax = ax.flatten()

    plot_ratios = ['1.000', '1.333', '3.000', '4.000']

    for i, ratio in enumerate(plot_ratios):
        coverage_file = os.path.join('run-ratio', f'{ratio}_412K', 'coverage.dat')
        header, coverage, ads_idx = parse_output.parse_coverage_file(coverage_file)

        plot_kwargs = {}
        if i != 1:
            plot_kwargs['legend'] = False
        if i == 0 or i == 1:
            plot_kwargs['xlabel'] = False
        if i == 1 or i == 3:
            plot_kwargs['ylabel'] = False

        plot_kwargs['xlim'] = [0, 150]

        plot_utils.plot_to_ax(ax[i], header, coverage, ads_idx, legend_kwargs={'fontsize': 7.5}, **plot_kwargs)
        ax[i].text(.02, .95, chr(97+i), weight='bold', ha='left', va='top', transform=ax[i].transAxes)

    if not os.path.isdir('figures'):
        os.mkdir('figures')

    fig.savefig('figures/ratio.png', bbox_inches='tight')
    fig.savefig('figures/ratio.pgf', bbox_inches='tight')

    logger.info('Done with ratio, written figures to figures/.')


def run_UB():
    """Explore the effect of theta_UB for surface reactions.
    """

    logger.info('Running UB...')

    UBs = np.arange(.35, 1.01, .05)

    batch_vars = {
        'UB': ['%.2f' % i for i in UBs]
    }

    mkmcxx.run_batch('input/input-template-UB.mkm', 'run-UB', batch_vars)

    logger.info('Plotting UB...')
    plot_utils.plot_coverage('run-UB')

    fig = plt.figure(figsize=(7, 3))
    gs = matplotlib.gridspec.GridSpec(3, 2, figure=fig, hspace=.15, wspace=.225, width_ratios=[.8, 1])
    subplots = [gs[:, 0], gs[0, 1], gs[1, 1], gs[2, 1]]
    ax = [fig.add_subplot(sp) for sp in subplots]

    plot_UBs = ['0.40', '0.70', '1.00']

    theta = np.linspace(-.1, 1.1, 121)
    def E_lat(UB): return (101 ** (theta / UB) - 1) / 100

    for i, UB in enumerate(plot_UBs):
        ax[0].plot(theta, E_lat(float(UB)))

        coverage_file = os.path.join('run-UB', f'{UB}_412K', 'coverage.dat')
        header, coverage, ads_idx = parse_output.parse_coverage_file(coverage_file)

        plot_kwargs = {
            'xlim': [0, 150]
        }
        if i != 0:
            plot_kwargs['legend'] = False
        if i != 2:
            plot_kwargs['xlabel'] = False

        ax[0].axvline(float(UB), c='grey', ls=':', lw=.5, label='_nolegend_')
        ax[i+1].axhline(float(UB), c='grey', ls=':', lw=.5, label='_nolegend_')

        plot_utils.plot_to_ax(ax[i+1], header, coverage, ads_idx, legend_kwargs={'fontsize': 7.5}, **plot_kwargs)
        ax[i+1].text(.015, .933, chr(98+i), weight='bold', ha='left', va='top', transform=ax[i+1].transAxes)

    ax[0].axhline(1, c='grey', ls=':', lw=.5, label='_nolegend_')
    ax[0].text(.02, .97, 'a', weight='bold', ha='left', va='top', transform=ax[0].transAxes)
    ax[0].set_xlim(-.05, 1.05)
    ax[0].set_ylim(-.05, 1.05)
    ax[0].set_xlabel(r'$\theta$')
    ax[0].set_ylabel(r'$E_\mathrm{lat}/E_{\theta=1}$')
    ax[0].legend([r'$\theta_\mathrm{UB}=' + '%.1f' % float(UB) + r'$' for UB in plot_UBs], loc='lower right')

    if not os.path.isdir('figures'):
        os.mkdir('figures')

    fig.savefig('figures/ub.pgf', bbox_inches='tight')
    fig.savefig('figures/ub.png', bbox_inches='tight')

    logger.info('Done with UB, written figures to figures/.')


def main():
    # perform the various runs

    # run 1: explore the effect of temperature on oscillations
    run_exploreT()

    # run 2: perform a sequence of various temperatures
    run_sequence()

    # run 3: compare large vs small time steps
    run_repeat()

    # run 4: explore the effect of pressure
    run_pressure()

    # run 5: explore the effect of NO : CO gas phase ratio
    run_ratio()

    # run 6: explore the effect of theta_UB for surface reactions
    run_UB()


if __name__ == '__main__':
    main()
