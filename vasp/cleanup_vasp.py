import os


os.chdir(os.path.dirname(os.path.abspath(__file__)))


def cleanup_single(dirname, keep_POSCAR):
    """Cleanup a VASP directory.

    Arguments:
        dirname {str} -- Location of VASP simulation directory.
        keep_POSCAR {bool} -- Whether to keep the POSCAR file.
    """
    # Cleanup a VASP directory.

    keep_files = ['INCAR', 'KPOINTS', 'POTCAR']
    if keep_POSCAR:
        keep_files.append('POSCAR')

    if not os.path.isdir(dirname):
        return

    for filename in os.listdir(dirname):
        if filename not in keep_files:
            os.remove(os.path.join(dirname, filename))


def main():
    for molecule in ['CO', 'NO']:
        cleanup_single(f'{molecule}-Pt100', True)
        cleanup_single(f'{molecule}-Pt100-vib', False)


if __name__ == '__main__':
    main()
