import os
import subprocess
import sys


vasp = 'bin/vasp_std'

os.chdir(os.path.dirname(os.path.abspath(__file__)))


def run_single(vasp, dirname):
    """Cleanup a VASP directory and then run the simulation in this directory.

    Arguments:
        vasp {str} -- Location of VASP executable.
        dirname {str} -- Location of VASP simulation directory.
    """

    # remove all output files
    keep_files = ['INCAR', 'KPOINTS', 'POSCAR', 'POTCAR']

    if not os.path.isdir(dirname):
        sys.exit(f'Simulation path {dirname} not valid.')

    for filename in os.listdir(dirname):
        if filename not in keep_files:
            os.remove(os.path.join(dirname, filename))

    # run VASP
    print(f'Running VASP simulation {dirname}...')

    proc = subprocess.run(['mpirun', vasp], cwd=dirname)

    if proc.returncode != 0:
        print('VASP returned an error. Exiting.')
        sys.exit(proc.returncode)


def main(vasp):
    """Run the VASP simulations to calculate the vibrational frequencies of
    adsorbed CO and NO.

    Arguments:
        vasp {str} -- Location of VASP executable.
    """

    if not os.path.isfile(vasp):
        sys.exit(f'VASP path {vasp} not valid.')

    vasp = os.path.abspath(vasp)

    for molecule in ['CO', 'NO']:
        relaxation_dir = f'{molecule}-Pt100'
        frequency_dir = f'{molecule}-Pt100-vib'

        relaxation_CONTCAR = os.path.join(relaxation_dir, 'CONTCAR')
        frequency_POSCAR = os.path.join(frequency_dir, 'POSCAR')

        # first, run relaxation simulation
        run_single(vasp, relaxation_dir)

        # copy CONTCAR from relaxation simulation to POSCAR for frequency simulation
        # note that the Pt atoms must be frozen in the frequency simulation,
        # so we do not do a direct copy but we read the file, freeze the Pt atoms,
        # and then write back.
        print(f'Copying CONTCAR for molecule {molecule}...')

        with open(relaxation_CONTCAR, 'r') as f:
            POSCAR = f.readlines()

        start_atoms = 9  # this is hardcoded

        num_Pt = int(POSCAR[6].split()[0])

        # freeze Pt atoms
        for i in range(start_atoms, start_atoms + num_Pt):
            POSCAR[i] = POSCAR[i].replace('T', 'F')

        # write back to POSCAR
        with open(frequency_POSCAR, 'w') as f:
            f.writelines(POSCAR)

        # now, run frequency simulation
        run_single(vasp, frequency_dir)


if __name__ == '__main__':
    main(vasp)
